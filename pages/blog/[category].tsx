import fs from "node:fs";
import path from "node:path"
import { GetStaticProps, GetStaticPaths } from "next";
import Link from "next/link";

interface Props {
    category: string
    contents: string[]
}

const BlogCategories = ({contents, category}:Props) => {
    return (
      <div>
        <h1 className="text-2xl font-semibold mb-4">{category}</h1>
        <ul>
          {contents.map((content) => (
            <li key={content}>
              <Link className="rounded-sm hover:text-white border border-black hover:bg-black px-4 py-2 transition duration-200" href={`/blog/${category}/${content}`}>
                {content.replace(/-/g, " ").toLowerCase().replace(/\d+/, '')}
              </Link>
            </li>
          ))}
        </ul>
      </div>
    );
};

export default BlogCategories;

export const getStaticPaths: GetStaticPaths = async () => {
  const categories = fs.readdirSync(path.join(process.cwd(), "pages", "blog")).filter(category => !category.includes('.tsx'))
  console.log(categories);
  
  return {
    paths: categories.map((category) => ({
      params: {
        category,
      },
    })),
    fallback: "blocking",
  };
};

export const getStaticProps: GetStaticProps = async (ctx) => {
  const category = ctx.params?.category as string;

    const filenames =  fs.readdirSync(path.join(process.cwd(),"pages","blog",  category))
    const contents = filenames.map(filename => filename.replace('.mdx', ''));
    
  return {
      props: {
        category,
      contents
    },
  };
};
