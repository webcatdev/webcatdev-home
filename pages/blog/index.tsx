import Head from "next/head";
import Link from "next/link";
import {
  SiJavascript,
  SiHtml5,
  SiCss3,
  SiPhp,
  SiNextdotjs,
  SiTailwindcss,
  SiNpm,
  SiComposer,
  SiSass,
  SiWindows,
  SiLinux,
  SiVirtualbox,
  SiDocker,
  SiExpress,
  SiMysql,
  SiMongodb,
  SiGit,
  SiTypescript,
  SiGithub,
  SiGitlab,
} from "react-icons/si";

export default function About() {
  return (
    <div className="py-4">
      <Head>
        <title>Webcatdev | Blog</title>
        <meta
          name="description"
          content="Blog page for webcatdev. My name is Hamza. I am a web developer from Turkey. I am interested in fullstack web development. I try to learn new things everyday."
        />
        <meta
          name="keywords"
          content="hamza eşsiz, web development, web developer, javascript, php, fullstack"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ul className="text-gray-900 grid grid-cols-2 gap-4">
        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/html"
          >
            <SiHtml5 />
            <span>HTML</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/css"
          >
            <SiCss3 />
            <span>CSS</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/javascript"
          >
            <SiJavascript />
            <span>JavaScript</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/git"
          >
            <SiGit />
            <span>Git</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/typescript"
          >
            <SiTypescript />
            <span>TypeScript</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/php"
          >
            <SiPhp />
            <span>PHP</span>
          </Link>
        </li>

        <li>
          <Link
            className="flex space-x-4 items-center py-2 px-4 border border-red-600 rounded-sm"
            href="/blog/linux"
          >
            <SiLinux />
            <span>Linux</span>
          </Link>
        </li>
      </ul>
    </div>
  );
}
