import Head from "next/head";
import Link from "next/link";
import { FaHandPeace } from 'react-icons/fa'
import { SiFuturelearn } from "react-icons/si";
import {FcAbout} from "react-icons/fc";


export default function Home() {

  return (
    <div className="py-4">
      <Head>
        <title>Webcatdev | Home</title>
        <meta
          name="description"
          content="Home page for webcatdev. My name is Hamza. I am a web developer from Turkey. I am interested in fullstack web development. I try to learn new things everyday."
        />
        <meta
          name="keywords"
          content="hamza eşsiz, web development, web developer, javascript, php, fullstack"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <section className="">
        <article>
          <h1 className="flex space-x-2 text-2xl font-semibold">
            <FaHandPeace /> <span>learn and help</span>
          </h1>
          <p className="max-w-screen-sm text-gray-900">
            I am a junior fullstack developer. This is my place to learn, teach
            and share. I am active on discord servers such as Patika.dev,
            Academind, Jonas Schmedtmann.
          </p>
          <div className="flex space-x-4 mt-4">
            <Link
              href="/blog"
              className="rounded-sm flex space-x-2 items-center bg-red-600 text-white px-4 py-2 border border-red-600"
            >
              <SiFuturelearn />
              <span>Start Learning</span>
            </Link>
            <Link
              href="/about"
              className="rounded-sm flex space-x-2 items-center px-4 py-2 border border-red-600 text-gray-900"
            >
              <FcAbout />
              <span>About Me</span>
            </Link>
          </div>
        </article>
      </section>
    </div>
  );
}
