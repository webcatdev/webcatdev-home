import Head from "next/head";
import {
  SiJavascript,
  SiHtml5,
  SiCss3,
  SiPhp,
  SiNextdotjs,
  SiTailwindcss,
  SiNpm,
  SiComposer,
  SiSass,
  SiWindows,
  SiLinux,
  SiVirtualbox,
  SiDocker,
  SiExpress,
  SiMysql,
  SiMongodb,
  SiGit,
  SiTypescript,
  SiGithub,
  SiGitlab,
} from "react-icons/si";
import { IoLogoLaravel } from "react-icons/io5";

export default function About() {
  return (
    <div className="py-4">
      <Head>
        <title>Webcatdev | About</title>
        <meta
          name="description"
          content="About page for webcatdev. My name is Hamza. I am a web developer from Turkey. I am interested in fullstack web development. I try to learn new things everyday."
        />
        <meta
          name="keywords"
          content="hamza eşsiz, web development, web developer, javascript, php, fullstack"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <section>
        <h1 className="flex space-x-2 text-2xl font-semibold">About me</h1>
        <p className="max-w-screen-sm text-gray-900 mb-4">
          I am Hamza. I was born in 1996. I from Turkey, Izmir. I started coding
          after my graduation from Ege University. I have been coding since
          2022. I focus on web development. Technologies i am learning:
        </p>
      </section>

      <section className="grid gap-4">
        <article>
          <h2 className="text-xl font-semibold">Languages</h2>
          <div className="flex space-x-4 my-2">
            <SiHtml5 className="w-9 h-9" aria-label="html5" title="html5" />
            <SiCss3 className="w-9 h-9" aria-label="css3" title="css3" />
            <SiJavascript
              className="w-9 h-9"
              aria-label="javascript"
              title="javascript"
            />
            <SiTypescript
              className="w-9 h-9"
              aria-label="typescript"
              title="typescript"
            />
            <SiPhp className="w-9 h-9" aria-label="php" title="php" />
          </div>
        </article>

        <article>
          <h2 className="text-xl font-semibold">Frameworks</h2>
          <div className="flex space-x-4 my-2">
            <SiNextdotjs
              className="w-9 h-9"
              aria-label="next.js"
              title="next.js"
            />
            <SiTailwindcss
              className="w-9 h-9"
              aria-label="tailwindcss"
              title="tailwindcss"
            />
            <IoLogoLaravel
              className="w-9 h-9"
              aria-label="laravel"
              title="laravel"
            />
            <SiExpress
              className="w-9 h-9"
              aria-label="express.js"
              title="express.js"
            />
          </div>
        </article>

        <article>
          <h2 className="text-xl font-semibold">Tools</h2>
          <div className="flex space-x-4 my-2">
            <SiGit className="w-9 h-9" aria-label="git" title="git" />
            <SiGithub className="w-9 h-9" aria-label="github" title="github" />
            <SiGitlab className="w-9 h-9" aria-label="gitlab" title="gitlab" />
            <SiNpm className="w-9 h-9" aria-label="npm" title="npm" />
            <SiComposer
              className="w-9 h-9"
              aria-label="composer"
              title="composer"
            />
            <SiSass className="w-9 h-9" aria-label="sass" title="sass" />
            <SiVirtualbox
              className="w-9 h-9"
              aria-label="virtualbox"
              title="virtualbox"
            />
            <SiDocker className="w-9 h-9" aria-label="docker" title="docker" />
          </div>

          <article>
            <h2 className="text-xl font-semibold">Databases</h2>
            <div className="flex space-x-4 my-2">
              <SiMysql className="w-9 h-9" aria-label="mysql" title="mysql" />
              <SiMongodb
                className="w-9 h-9"
                aria-label="mongodb"
                title="mongodb"
              />
            </div>
          </article>

          <article>
            <h2 className="text-xl font-semibold">Environments</h2>
            <div className="flex space-x-4 my-2">
              <SiWindows
                className="w-9 h-9"
                aria-label="windows"
                title="windows"
              />
              <SiLinux className="w-9 h-9" aria-label="linux" title="linux" />
            </div>
          </article>
        </article>
      </section>
    </div>
  );
}
