import Link from "next/link";
import React, { useEffect, useState } from "react";
import { SiGithub, SiGitlab } from "react-icons/si";
import { GiHollowCat } from "react-icons/gi";
import { Links } from "../types/Navigation";
import Navigation from "./Navigation";
import { Breadcrumbs } from "@mui/material";
import { useRouter } from "next/router";
import { FcOpenedFolder, FcFolder } from "react-icons/fc";
import { AiFillRocket } from "react-icons/ai";
interface Props {
  children: React.ReactNode;
}

const Layout: React.FC<Props> = ({ children }) => {
  const router = useRouter();
  const [shouldShowGoTop, setShouldShowGoTop] = useState(false);

  useEffect(() => {
    function shouldShow() {
      const bodyHeight = Math.floor(Number(window.getComputedStyle(document.body).height.replace('px', '')));
      if (bodyHeight > window.innerHeight) {        
        setShouldShowGoTop(true);
        return true;
      }

      setShouldShowGoTop(false);
      return true
    }

    shouldShow()

    router.events.on('routeChangeComplete', shouldShow)
    window.addEventListener("resize", shouldShow);
  }, []);

  const mainNavLinks: Links = [
    {
      route: "/",
      name: "Home",
    },
    {
      route: "/about",
      name: "About",
    },
    {
      route: "/blog",
      name: "Blog",
    },
  ];

  const routes =
    router.asPath.includes("#") || router.asPath.includes("?")
      ? router.pathname.split("/")
      : router.asPath.split("/");

  return (
    <div className="min-h-screen max-w-screen-lg mx-auto px-4 flex flex-col">
      <header className="py-4 flex justify-between items-center">
        <Link href="/">
          <div
            className="flex space-x-2 items-center text-2xl font-bold text-red-600 cursor-pointer"
            aria-label="logo"
          >
            <span>webcatdev</span>
            <GiHollowCat aria-hidden />
          </div>
        </Link>

        <Navigation navLabel="main-navigation" links={mainNavLinks} />
      </header>
      <main className="py-2">
        {router.asPath !== "/" && (
          <Breadcrumbs>
            {routes.map((route, i) =>
              route == "" ? (
                <Link
                  key="home"
                  className="flex items-center space-x-1"
                  href="/"
                >
                  <FcFolder /> <span>home</span>
                </Link>
              ) : (
                <Link
                  key={route}
                  className="flex items-center space-x-1"
                  href={routes.slice(0, i + 1).join("/")}
                >
                  {i == routes.length - 1 ? <FcOpenedFolder /> : <FcFolder />}{" "}
                  <span>{route}</span>
                </Link>
              )
            )}
          </Breadcrumbs>
        )}
        {children}
      </main>

      <footer className="bg-gray-100 mt-auto flex justify-center space-x-4 py-4">
        <a
          aria-label="Github"
          title="Github"
          href="https://github.com/webCatDev"
          target="_blank"
          rel="noreferrer noopener"
        >
          <SiGithub className="w-9 h-9 hover:text-red-600" aria-hidden />
        </a>
        <a
          aria-label="Gitlab"
          title="Gitlab"
          href="https://gitlab.com/webcatdev"
          target="_blank"
          rel="noreferrer noopener"
        >
          <SiGitlab className="w-9 h-9 hover:text-red-600" aria-hidden />
        </a>

        {shouldShowGoTop && (
          <a
            href="#"
            className="fixed right-2 bottom-2 bg-gray-100 hover:bg-yellow-400 p-2 rounded-full"
          >
            <AiFillRocket
              className="w-9 h-9"
              aria-label="go top"
              title="go top"
            />
          </a>
        )}
      </footer>
    </div>
  );
};

export default Layout;
