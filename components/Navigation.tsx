import Link from "next/link";
import { useRouter } from "next/router";
import { Links } from "../types/Navigation";

interface Props {
  navLabel: string;
  links: Links;
}

const Navigation: React.FC<Props> = ({ navLabel, links }) => {
  const { pathname } = useRouter();

  return (
    <nav role="navigation" aria-label={navLabel}>
      <ul className="list-none flex space-x-4">
        {links.map((link) => (
          <li key={link.route}>
            <Link
              className={`p-2 rounded-sm hover:bg-red-100 ${
                link.route === pathname
                  ? "bg-red-600 text-white hover:bg-red-600"
                  : ""
              }`}
              href={link.route}
            >
              {link.name}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};

export default Navigation;
