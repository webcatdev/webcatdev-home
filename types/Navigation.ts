export interface Link {
  route: string;
  name: React.ReactNode;
}

export type Links = Link[]
